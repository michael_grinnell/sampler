# Simple Sampler
Simple Sampler is a VST3 plugin built in C++ using the Juce Audio Framework.

## Installation
To use just drop the VST folder into your plugin directory and open with your favourite DAW.
Please note that this Repo doesn't contain the builds folder. Therefore you will need to build the project from the [Projucer](https://juce.com/discover/projucer) first.

## Features
![Simple Sampler](http://mggames.eu/img/SimpleSampler.png)
### General
- This plugin takes MIDI as input, You will need to place it on a midi track in your favourite DAW.
- The Gain knob controls the overall volume of the sampler
- To Load A sample, Either drop a file into the sample area, it click to open a file browser to choose a file.
- The Audio formats accepted are mp3, wav, and aif, the loaded sample must also be less than 15 seconds.

### Transport Controls
- The Start and End knobs control the start and end position of the sample. The start position is represented by the green line on the waveform, the end is represented by the red. The current position is the white line that is drawn as a sample plays.
- The Speed knob controls the playback speed, this also affects the pitch of the sample.
- The Direction Button reverses the currently loaded sound.
- The Mode Button turns on and off loop mode, when looping is enabled, the playback will return to the start position of the loaded sound once it reaches the end.

### Envelope Controls
- Attack, Decay, Sustain, Release
- This is a simple ADSR amplitude envelope that controls the time it takes for the sound to reach full volume and then fade out, these values are in seconds.

### Filter Controls
- There are two multi-mode ladder filters based on the Moog Ladder Filter.
- The lowpass and highpass can be combined to create a bandpass.
- The Rolloff Button changes the steepness of the filter roll-off curve between 12 and 24 DB per octave.

## Main Classes

- MainProcesser

This class was created to separate the main processing functionality from the SamplerAudioProcessor class.
The Idea is that the SamplerAudioProcessor class can be responsible for communicating with the DAW and this class can be responsible for the main audio processing.
This should improve readability as the SamplerAudioProcessor class was getting quite large

- SampleSound

This class is based on the Juce SamplerSound Class.
The functionality of the SamplerSoun class has been extended to allow manipulation of transport controls
such as the Start/End position and the playback speed

- SampleVoice

This class is based on the Juce SamplerVoice Class.
This class is responsible for playing the samplerSound class, The main processing of an instance of a SampleSound happens in its renderNextBlock method.

- MainComponent

This is a container class for all of the other components.

- SliderGroup

This Class Stores a group of Sliders and associated labels.
It handles the positioning of slider components equal to each other. It also makes adding new groups of sliders Simpler to implement. Just pass the slider group a name and a set of names for the each slider
Then attach the names to the processer Tree in the plugin Processer

- ImageToggleButton

This Class Is Basically a Toggle Button that Switches Between Two Images. The Parameters are stored in the ButtonParams Struct. It contains two images, A parameter Name, and Text for the on and off state

- AudioThumbnailComponent

This Class is responsible for the drawing of the audio waveform, It also draws a line three lines to represent the start, end and current position of the loaded sample.

## Demo
Here is a short [Screencast ](www.youtube.com)demonstrating the Application as well as a brief overview of the code.

## Contributing
Pull requests are welcome. For any changes, please contact me first to discuss what you would like to change.

## Acknowledgements 
This Project was created with the help of The Audio Programmer's YouTube Channel and his playlist on creating a VST Sampler. Thank you to the Juce Developers for providing the open-source framework.

Thank you to Rory Walsh and Dundalk Institute of Technology for teaching the Core Framework, and providing insight and resources for this project.
    
[The Audio Programmer](https://www.youtube.com/playlist?list=PLLgJJsrdwhPyTmhbSVVa5tPmJXJE-OMYm)
    
[Juce Tutorials](https://juce.com/learn/tutorials.html)

[Tutorial: Draw audio waveforms](https://docs.juce.com/master/tutorial_audio_thumbnail.html)

[Tutorial: Build an audio player](https://docs.juce.com/master/tutorial_playing_sound_files.html)

## License
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)