#include <JuceHeader.h>
#include "AudioThumbnailComponent.h"

#pragma region Constructor/Destructor

AudioThumbnailComponent::AudioThumbnailComponent(MainProcesser& p) :
    processor(p),
    mThumbnail(p.GetAudioThumbnail()),
    mSampleParams(p.GetLoadedSample()),
    mGainAttachment(processor.GetAPVTS(), "GAIN", mGainSlider)
{
    AttachGainSlider();
}

AudioThumbnailComponent::~AudioThumbnailComponent() {}

//Sets the bounds for the Audio Waveform inside the thumnail component
void AudioThumbnailComponent::SetThumbnailBounds()
{
    thumbnailBounds = (Rectangle<int>(
        COMPONENT_PADDING_MED, //X
        (COMPONENT_PADDING_LARGE * 1.5),//Y
        getWidth() - (COMPONENT_PADDING_MED * 2),//WIDTH
        getHeight() - (COMPONENT_PADDING_LARGE * 2)));//HEIGHT
}

void AudioThumbnailComponent::AttachGainSlider()
{
    mGainSlider.setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
    mGainSlider.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
    mGainSlider.setPopupDisplayEnabled(true, false, this);
    mGainSlider.setTextValueSuffix("%");
    addAndMakeVisible(mGainSlider);

    mGainLabel.SetText("Gain");
    addAndMakeVisible(mGainLabel);
}
#pragma endregion

#pragma region Paint 

void AudioThumbnailComponent::paint(Graphics& g)
{
    g.fillAll(Colour(COMPONENT_BG_COLOUR));

    if (thumbnailBounds.isEmpty())
        SetThumbnailBounds();

    if (mThumbnail.getNumChannels() == 0)
        paintIfNoFileLoaded(g);
    else
        paintIfFileLoaded(g);

    paintName(g);

    paintGainSLider(g);
}

void AudioThumbnailComponent::paintGainSLider(Graphics& g)
{
}

void AudioThumbnailComponent::paintIfNoFileLoaded(Graphics& g)
{
    g.fillRect(thumbnailBounds);
    g.setColour(Colour(BORDER_COLOUR));
    g.drawFittedText(NO_FILE_TEXT, thumbnailBounds, Justification::centred, 1);
}

void AudioThumbnailComponent::paintIfFileLoaded(Graphics& g)
{
    paintAudioThumbnail(g);

    paintTransportLines(g);

    mSampleParams = processor.GetLoadedSample();
}

void AudioThumbnailComponent::paintAudioThumbnail(Graphics& g)
{
    g.fillRect(thumbnailBounds);
    g.setColour(Colour(THUMBNAIL_COLOUR));

    mThumbnail.drawChannels(g,
        thumbnailBounds,
        0.0,
        mThumbnail.getTotalLength(),
        1.0f);
}

void AudioThumbnailComponent::paintTransportLines(Graphics& g)
{
    float currentPosition = processor.GetTransportPosition();
    float startPosition = ((float)mSampleParams.startSample / (float)mSampleParams.samplerate);
    float endPosition = ((float)mSampleParams.endSample / (float)mSampleParams.samplerate);

    if (currentPosition + startPosition < endPosition)
    {
        g.setColour(Colour(PLAYHEAD_COLOUR));
        paintTransportLine(g, currentPosition + startPosition);
    }

    g.setColour(Colours::green);
    paintTransportLine(g, startPosition);

    g.setColour(Colours::red);
    paintTransportLine(g, endPosition);
}

void AudioThumbnailComponent::paintTransportLine(Graphics& g, float adjustedPosition)
{
    float totalLength = mThumbnail.getTotalLength();

    auto drawPosition((adjustedPosition / totalLength) * thumbnailBounds.getWidth()
        + thumbnailBounds.getX());

    //draw a line that is 2 pixels wide between the top (y) and bottom of the rectangle.
    g.drawLine(drawPosition, thumbnailBounds.getY(), drawPosition,
        thumbnailBounds.getBottom(), 2.0f);
}

void AudioThumbnailComponent::paintName(Graphics& g)
{
    g.setColour(Colour(BORDER_COLOUR));
    g.drawRect(thumbnailBounds, BORDER_SMALL);

    g.setColour(Colour(TEXT_COLOUR));
    g.setFont(FONT_MED);
    g.drawText(mSampleParams.name, 0, (COMPONENT_PADDING_LARGE * .75) - (FONT_MED / 2), getWidth(), getHeight(), Justification::centredTop);

    g.setColour(Colour(BORDER_COLOUR));

    Rectangle<int> componentBorder(0, 0, getWidth(), getHeight());
    g.drawRect(componentBorder, BORDER_LARGE);
}

#pragma endregion

#pragma region Events

void AudioThumbnailComponent::resized() {
    mGainSlider.setBounds(thumbnailBounds.getX() + BORDER_LARGE - 1, getHeight() / 15, getWidth() / 10, getHeight() / 5);
    mGainLabel.setBounds(thumbnailBounds.getX()+ BORDER_LARGE -1, BORDER_LARGE -1, getWidth()/10 , getHeight()/15);
}

bool AudioThumbnailComponent::isInterestedInFileDrag(const StringArray& files)
{
    bool valid = false;
  
    for (auto file : files)
        valid = validFile(file);

    return valid;
}

void AudioThumbnailComponent::filesDropped(const StringArray& files, int x, int y)
{
    for (auto file : files)
    {
        if (isInterestedInFileDrag(file))
        {
            processor.LoadFile(file);
            startTimer(20);
        }
    }

    mSampleParams = processor.GetLoadedSample();
    repaint();
}

//This should Probably be in a Utility Class
bool AudioThumbnailComponent::validFile(String file)
{
    //TODO - Add more File Types in Constants 
    String ext = File(file).getFileExtension();

    if (ext.toLowerCase() == ".wav" ||
        ext.toLowerCase() == ".mp3" ||
        ext.toLowerCase() == ".aif" ||
        ext.toLowerCase() == ".aiff")
    {
        return true;
    }

    AlertWindow alert("Invalid File", "Please Drop Wav, AIF, or MP3", AlertWindow::AlertIconType::WarningIcon, this);
    alert.addButton("Cool", 0);
    alert.runModalLoop();

    return false;
}

void AudioThumbnailComponent::timerCallback()
{
    repaint();
}

void AudioThumbnailComponent::mouseDown(const MouseEvent& event)
{
    if (thumbnailBounds.contains(event.x, event.y))
    {
        // Opens a Dialog to allow the user to load a file
        FileChooser chooser("Please Choose An Audio File", File::getSpecialLocation(File::userDocumentsDirectory), "*.wav; *.mp3");

        // if file is selected
        if (chooser.browseForFileToOpen())
        {
            String path = chooser.getResult().getFullPathName();
            if (validFile(path))
            {
                processor.LoadFile(path);
                startTimer(20);
            }

        }
    }

    mSampleParams = processor.GetLoadedSample();
    repaint();
}

#pragma endregion
