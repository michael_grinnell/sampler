#pragma once

#include <JuceHeader.h>
#include "MainProcesser.h"
#include "SliderGroup.h"

class AudioThumbnailComponent    : 
    public Component,
    //public MouseListener,
    public FileDragAndDropTarget,
    private Timer
{
public:
    AudioThumbnailComponent(MainProcesser& p);
    ~AudioThumbnailComponent();

    void SetThumbnailBounds();

    void AttachGainSlider();

    void paint (Graphics&) override;

    void paintGainSLider(Graphics& g);

    void paintIfNoFileLoaded(Graphics& g);
    void paintIfFileLoaded(Graphics& g);

    void paintAudioThumbnail(Graphics& g);
    void paintTransportLines(Graphics& g);
    void paintTransportLine(Graphics& g, float adjustedPosition);

    void paintName(Graphics& g);

    void resized() override;
    bool isInterestedInFileDrag(const StringArray& files) override;
    void filesDropped(const StringArray& files, int x, int y) override;
    bool validFile(String file);
    void timerCallback() override;

    void mouseDown(const MouseEvent& event) override;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioThumbnailComponent)

    MainProcesser& processor;
    AudioThumbnail& mThumbnail;
    SampleParams& mSampleParams;

    Rectangle<int> thumbnailBounds;

    Slider mGainSlider;
    TextLabel mGainLabel;
    AudioProcessorValueTreeState::SliderAttachment mGainAttachment;
};
