#include <JuceHeader.h>
#include "ButtonGroup.h"

ButtonGroup::ButtonGroup(MainProcesser& p, String name) : processor(p), mName(name) {}

ButtonGroup::~ButtonGroup(){}

void ButtonGroup::SetButtons(std::vector<ButtonParams> params)
{
    for (size_t i = 0; i < params.size(); ++i)
    {
        mButtons.add(std::unique_ptr<ImageToggleButton>(new ImageToggleButton(params[i])));
        addAndMakeVisible(mButtons[i]);

        mButtonAttachments.add(
            new AudioProcessorValueTreeState::ButtonAttachment(
                processor.GetAPVTS(),
                (mName + "_" + mButtons[i]->GetName()).toUpperCase(),
                *mButtons[i]));
    }
}

void ButtonGroup::paint (Graphics& g){}

void ButtonGroup::resized()
{
    float buttonWidth = getWidth() / mButtons.size();
    float buttonHeight = getHeight();

    for (size_t i = 0; i < mButtons.size(); ++i)
    {
        mButtons[i]->setBounds(buttonWidth * i, 0, buttonWidth, buttonHeight);
    }
}