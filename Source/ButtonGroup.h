#pragma once

#include <JuceHeader.h>
#include "ImageToggleButton.h"
#include "MainProcesser.h"

//==============================================================================
//This is a container class that holds a group of Sampler Image Buttons
//The class handles spacing and will paint each button spaced equally

class ButtonGroup    : public Component
{
public:
    ButtonGroup(MainProcesser& p, String name);
    ~ButtonGroup();

    void SetButtons(std::vector<ButtonParams> params);

    void paint (Graphics&) override;
    void resized() override;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ButtonGroup)

    MainProcesser& processor;
    String mName;

    OwnedArray<ImageToggleButton> mButtons;
    OwnedArray<AudioProcessorValueTreeState::ButtonAttachment> mButtonAttachments;
};
