#pragma once

// GUI
const unsigned short PLUGIN_WIDTH = 700;
const unsigned short PLUGIN_HEIGHT = 450;

const unsigned char COMPONENT_PADDING = 10;
const unsigned char COMPONENT_PADDING_MED = 15;
const unsigned char COMPONENT_PADDING_LARGE = 40;

const unsigned char BORDER_SMALL = 1;
const unsigned char BORDER_MED = 2;
const unsigned char BORDER_LARGE = 3;


// TEXT
const float FONT_LARGE = 25.f;
const float FONT_MED = 18.f;
const float FONT_SMALL = 12.f;
const float FONT_EXTRA_SMALL = 6.f;

const float LINE_SIZE = 5.f;


// SLIDERS
const unsigned char TEXT_BOX_WIDTH = 40;
const unsigned char TEXT_BOX_HEIGHT = 20;

const float START_X = 0.1f;
const float START_Y = 0.1f;
const float SLIDER_WIDTH = 0.2f;
const float SLIDER_HEIGHT = 0.75f;

//Colours
const uint32 BG_COLOUR = 0xff000000;
const uint32 BORDER_COLOUR = 0xff989fb3;
const uint32 COMPONENT_BG_COLOUR = 0x00565656;
const uint32 COMPONENT_BG_COLOUR_LIGHT = 0x002e3847;
const uint32 TEXT_COLOUR = 0xffffffff;

const uint32 IMAGE_COLOUR_NORMAL = 0xffffffff;
const uint32 IMAGE_COLOUR_OVER = 0xffffffff;
const uint32 IMAGE_COLOUR_DOWN = 0xffffffff;

const uint32 THUMBNAIL_COLOUR = 0xffee7f1b;
const uint32 PLAYHEAD_COLOUR = 0xffffffff;


//SYNTH
const unsigned char SYNTH_VOICES = 1;
const unsigned char MIDDLE_C = 60;

//AUDIO
const int SAMPLERATE = 44100;
const int MAX_SAMPLE_LENGTH_SECONDS = 15;

//TEXT
const String NO_FILE_TEXT = "Click Or Drop A File Here To Load";