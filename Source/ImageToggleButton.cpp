#include <JuceHeader.h>
#include "ImageToggleButton.h"
#include "Constants.hpp"

#pragma region Constructor/Destructor

ImageToggleButton::ImageToggleButton(ButtonParams params) :
    mParams(params)
{
    setButton();
}

ImageToggleButton::~ImageToggleButton() {}

void ImageToggleButton::mouseDown(const MouseEvent& event)
{
    HandleStateChange();
}

#pragma endregion

#pragma region Setters

void ImageToggleButton::setOnImage(juce::Image& image)
{
    jassert(image.isValid());
    mParams.onImage = image;
}

void ImageToggleButton::setOffImage(juce::Image& image)
{
    jassert(image.isValid());
    mParams.offImage = image;
}

void ImageToggleButton::setButton()
{
    mButton.setImages(false, true, true,
        mParams.onImage, 1.0f, Colour(IMAGE_COLOUR_NORMAL),
        mParams.onImage, 1.0f, Colour(IMAGE_COLOUR_OVER),
        mParams.onImage, 1.0f, Colour(IMAGE_COLOUR_DOWN),
        0.5f);

    //I want buttons on by default
    mButton.setToggleState(true, NotificationType::dontSendNotification);

    mButton.onClick =
        [&]() { HandleStateChange(); };

    mLabel.SetText(mParams.name);

    addAndMakeVisible(mButton);
    addAndMakeVisible(mLabel);
}

void ImageToggleButton::setButtonText(String onText, String offText)
{
    mParams.onText = onText;
    mParams.offText = offText;
}

#pragma endregion

void ImageToggleButton::paint (Graphics& g)
{
    g.fillAll(Colour(COMPONENT_BG_COLOUR));

    Colour(BG_COLOUR);
    g.fillRect(0.f, 0.f, (float)getWidth(), getHeight() * 0.15f);

    g.setColour(Colour(BORDER_COLOUR));
    g.drawRect(0.f, 0.f, (float)getWidth(), getHeight() * 0.15f);

    g.setColour(Colour(TEXT_COLOUR));
    g.setFont(FONT_SMALL);

    g.drawText((mIsOn ? mParams.onText : mParams.offText), 0, 0, getWidth(), getHeight(), Justification::centredTop);

    g.setColour(Colour(BORDER_COLOUR));
    g.drawRect(0, 0, getWidth(), getHeight());
}

void ImageToggleButton::resized()
{
    mButton.setBoundsRelative(0, 0.20f, 1.f, 0.50f);
    mLabel.setBoundsRelative(0, 0.75f, 1.f, 0.25f);
}

//Toggles Between On and Off Image
void ImageToggleButton::HandleStateChange()
{
 
    triggerClick();
    

    if (mParams.offImage.isNull())
        return;

    if (mIsOn = !mIsOn)
    {
        mButton.setImages(false, true, true,
            mParams.onImage, 1.0f, Colour(IMAGE_COLOUR_NORMAL),
            mParams.onImage, 1.0f, Colour(IMAGE_COLOUR_OVER),
            mParams.onImage, 1.0f, Colour(IMAGE_COLOUR_DOWN),
            0.5f);
    }
    else
    {
        mButton.setImages(false, true, true,
            mParams.offImage, 1.0f, Colour(IMAGE_COLOUR_NORMAL),
            mParams.offImage, 1.0f, Colour(IMAGE_COLOUR_OVER),
            mParams.offImage, 1.0f, Colour(IMAGE_COLOUR_DOWN),
            0.5f);
    }
}