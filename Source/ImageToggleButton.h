#pragma once

#include <JuceHeader.h>
#include "TextLabel.h"

//==============================================================================
//This Class Is Basically a Toggle Button that Switches Between Two Images
//The Parameters are stored in the ButtonParams Struct
//It contains Two images, A parameter Name, and Text for the on and off state
//This class would be better named ImageToggleButton
struct ButtonParams
{
    String name, onText, offText;
    juce::Image onImage, offImage;
    ButtonParams(String name, String onText, String offText, juce::Image onImage, juce::Image offImage)
        : name(name),
        onText(onText),
        offText(offText),
        onImage(onImage),
        offImage(offImage)
    {}
};

class ImageToggleButton : public ToggleButton
{
public:
    ImageToggleButton(ButtonParams mParams);
    ~ImageToggleButton();

    void mouseDown(const MouseEvent& event) override;

    void setOnImage(juce::Image& image);
    void setOffImage(juce::Image& image);
    void setButton();
    void setButtonText(String text,String offText);

    void paint (Graphics&) override;
    void resized() override;
    void HandleStateChange();

    String GetName() { return mParams.name; };

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ImageToggleButton)

    ButtonParams mParams;
    ImageButton mButton;
    TextLabel mLabel;
    bool mIsOn{true};
};
