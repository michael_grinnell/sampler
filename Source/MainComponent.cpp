#include <JuceHeader.h>
#include "MainComponent.h"

MainComponent::MainComponent(MainProcesser& p) :
    processor(p),
    mAudioThumb(p),
    mAmpEnvControls(p, "ENVELOPE"),
    mLPControls(p, "LOWPASS"),
    mHPControls(p, "HIGHPASS"),
    mTransportControls(p, "TRANSPORT"),
    mLoopControls(p,"BUTTONS")
{
    MakeComponantsVisable();
    SetParameters();
}

MainComponent::~MainComponent(){}

void MainComponent::paint (Graphics& g){}

void MainComponent::resized()
{
    background.setBoundsRelative(0, 0, 1, 1);
    mTransportControls.setBoundsRelative(0.f, 0.f, 0.5f, 0.25f);
    mLoopControls.setBoundsRelative(0.5f, 0.f, 0.5f, 0.25f);
    mAudioThumb.setBoundsRelative(0.f, 0.25f, 1.f, 0.50f);
    mAmpEnvControls.setBoundsRelative(0.f, 0.75f, 0.50f, 0.25f);
    mHPControls.setBoundsRelative(0.75f, 0.75f, 0.25f, 0.25f);
    mLPControls.setBoundsRelative(0.50f, 0.75f, 0.25f, 0.25f);
}

void MainComponent::SetParameters()
{
    background.setImage(ImageCache::getFromMemory(BinaryData::background_texture_png, BinaryData::background_texture_pngSize), RectanglePlacement::fillDestination);

    mAmpEnvControls.SetSliders(std::vector<String> {"Attack s", "Decay s", "Sustain  s", "Release s"});
    mLPControls.SetSliders(std::vector<String> {"Freq hz", "Res hz", "Drive hz"});
    mHPControls.SetSliders(std::vector<String> {"Freq hz", "Res hz", "Drive hz"});
    mTransportControls.SetSliders(std::vector<String> {"Start %", "End %", "Speed %"});
    SetImageButtons();
}

void MainComponent::SetImageButtons()
{
    //Load button images
    juce::Image forward_arrow = ImageCache::getFromMemory(BinaryData::Forward_Arrow_png, BinaryData::Forward_Arrow_pngSize);
    juce::Image backward_arrow = ImageCache::getFromMemory(BinaryData::Backward_Arrow_png, BinaryData::Backward_Arrow_pngSize);

    juce::Image two_forward_arrow = ImageCache::getFromMemory(BinaryData::Two_Arrow_Forward_png, BinaryData::Two_Arrow_Forward_pngSize);
    juce::Image loop_arrow = ImageCache::getFromMemory(BinaryData::Loop_Arrow_png, BinaryData::Loop_Arrow_pngSize);

    juce::Image curved_arrow = ImageCache::getFromMemory(BinaryData::Curved_Arrow_png, BinaryData::Curved_Arrow_pngSize);
    juce::Image two_curved_arrow = ImageCache::getFromMemory(BinaryData::Two_Curved_Arrow_png, BinaryData::Two_Curved_Arrow_pngSize);

    //Create array of button parameters
    std::vector<ButtonParams> mLoopButtonParams
    {
        ButtonParams("Direction", "Forward", "Reverse", forward_arrow, backward_arrow),
        ButtonParams("Mode", "Single", "Loop", two_forward_arrow, loop_arrow),
        ButtonParams("Rolloff", "12db", "24db", curved_arrow, two_curved_arrow)
    };

    //Send button params to button group
    mLoopControls.SetButtons(mLoopButtonParams);
}

void MainComponent::MakeComponantsVisable()
{
    addAndMakeVisible(background);
    addAndMakeVisible(mAudioThumb);
    addAndMakeVisible(mAmpEnvControls);
    addAndMakeVisible(mHPControls);
    addAndMakeVisible(mLPControls);
    addAndMakeVisible(mLoopControls);
    addAndMakeVisible(mTransportControls);
}
