#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "AudioThumbnailComponent.h"
#include "SliderGroup.h"
#include "ButtonGroup.h"

//This is a container class for all other components
class MainComponent    : public Component
{

public:
    MainComponent(MainProcesser& p);
    ~MainComponent();

    void paint (Graphics&) override;
    void resized() override;

private:
    void SetParameters();
    void SetImageButtons();
    void MakeComponantsVisable();

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)

    MainProcesser& processor;

    SliderGroup mAmpEnvControls;
    SliderGroup mLPControls;
    SliderGroup mHPControls;

    SliderGroup mTransportControls;
    ButtonGroup mLoopControls;

    AudioThumbnailComponent mAudioThumb;
    ImageComponent background;
};
