#include "MainProcesser.h"
#include "PluginProcessor.h"

#pragma region Initialization

MainProcesser::MainProcesser(SamplerAudioProcessor& p) : processer(p),
    mAPVTS(p, nullptr, "PARMETERS", CreateParameters()),
    thumbnailCache(5),
    thumbnail(64, formatManager, thumbnailCache), 
    mLoadedSample("", 0, 0)
{
    Initialize();
}

MainProcesser::~MainProcesser()
{
    transportSource.releaseResources();
}

void MainProcesser::Initialize()
{
    for (size_t i = 0; i < 8; i++)
    {
        mSampler.addVoice(new SampleVoice());
    }
    
    mSampler.setCurrentPlaybackSampleRate(SAMPLERATE);

    formatManager.registerBasicFormats();
    transportSource.addChangeListener(this);

    RegisterListeners();
}

void MainProcesser::prepareToPlay(double sampleRate, int samplesPerBlock)
{
    transportSource.prepareToPlay(samplesPerBlock, sampleRate);
    mSampler.setCurrentPlaybackSampleRate(sampleRate);

    UpdateADSR();
    UpdateFilter();

    dsp::ProcessSpec spec;
    spec.sampleRate = sampleRate;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = processer.getTotalNumOutputChannels();

    mLowPassFilter.prepare(spec);
    mLowPassFilter.reset();

    mHighPassFilter.prepare(spec);
    mHighPassFilter.reset();
}

AudioProcessorValueTreeState::ParameterLayout MainProcesser::CreateParameters()
{
    std::vector<std::unique_ptr<RangedAudioParameter>> parameters;

    //ADSR ENVELOPE
    parameters.push_back(std::make_unique<AudioParameterFloat>("ENVELOPE_ATTACK", "Attack", 0.f, 5.f, 2.f));
    parameters.push_back(std::make_unique<AudioParameterFloat>("ENVELOPE_DECAY", "Decay", 0.f, 3.f, 2.f));
    parameters.push_back(std::make_unique<AudioParameterFloat>("ENVELOPE_SUSTAIN", "Sustain", 0.f, 1.f, 1.f));
    parameters.push_back(std::make_unique<AudioParameterFloat>("ENVELOPE_RELEASE", "Release", 0.f, 5.f, 2.f));

    //LOW PASS FILTER
    parameters.push_back(std::make_unique<AudioParameterInt>("LOWPASS_FREQ", "Freq", 20, 20000, 20000));
    parameters.push_back(std::make_unique<AudioParameterFloat>("LOWPASS_RES", "Res", 0.1f, 1.f, 0.25f));
    parameters.push_back(std::make_unique<AudioParameterFloat>("LOWPASS_DRIVE", "Drive", 1.f, 10.f, 1.1f));

    //HIGH PASS FILTER
    parameters.push_back(std::make_unique<AudioParameterInt>("HIGHPASS_FREQ", "Freq", 20, 20000, 20));
    parameters.push_back(std::make_unique<AudioParameterFloat>("HIGHPASS_RES", "Res", 0.1f, 1.f, 0.25f));
    parameters.push_back(std::make_unique<AudioParameterFloat>("HIGHPASS_DRIVE", "Drive", 1.f, 10.f, 1.1f));

    //TRANSPORT
    parameters.push_back(std::make_unique<AudioParameterInt>("TRANSPORT_START", "Start", 0, 100, 0.f));
    parameters.push_back(std::make_unique<AudioParameterInt>("TRANSPORT_END", "End", 0, 100, 100));
    parameters.push_back(std::make_unique<AudioParameterInt>("TRANSPORT_SPEED", "Speed", 50, 500, 100));

    //BUTTONS
    parameters.push_back(std::make_unique<AudioParameterBool>("BUTTONS_DIRECTION", "Direction", true));
    parameters.push_back(std::make_unique<AudioParameterBool>("BUTTONS_MODE", "Mode", true));
    parameters.push_back(std::make_unique<AudioParameterBool>("BUTTONS_ROLLOFF", "Rolloff", true));

    parameters.push_back(std::make_unique<AudioParameterInt>("GAIN", "Gain", 0, 100, 50));

    return { parameters.begin(), parameters.end() };
}

void MainProcesser::RegisterListeners()
{
    //ADSR ENVELOPE
    mAPVTS.addParameterListener("ENVELOPE_ATTACK", this);
    mAPVTS.addParameterListener("ENVELOPE_DECAY", this);
    mAPVTS.addParameterListener("ENVELOPE_SUSTAIN", this);
    mAPVTS.addParameterListener("ENVELOPE_RELEASE", this);

    //LOW PASS FILTER
    mAPVTS.addParameterListener("LOWPASS_FREQ", this);
    mAPVTS.addParameterListener("LOWPASS_RES", this);
    mAPVTS.addParameterListener("LOWPASS_DRIVE", this);

    //HIGH PASS FILTER
    mAPVTS.addParameterListener("HIGHPASS_FREQ", this);
    mAPVTS.addParameterListener("HIGHPASS_RES", this);
    mAPVTS.addParameterListener("HIGHPASS_DRIVE", this);

    //TRANSPORT
    mAPVTS.addParameterListener("TRANSPORT_START", this);
    mAPVTS.addParameterListener("TRANSPORT_END", this);
    mAPVTS.addParameterListener("TRANSPORT_SPEED", this);

    //BUTTONS
    mAPVTS.addParameterListener("BUTTONS_DIRECTION", this);
    mAPVTS.addParameterListener("BUTTONS_MODE", this);
    mAPVTS.addParameterListener("BUTTONS_ROLLOFF", this);

    mAPVTS.addParameterListener("GAIN", this);
}

#pragma endregion

#pragma region Process

double MainProcesser::GetTransportPosition()
{
    double position = 0;
    bool voiceFound = false;
    int voiceIndex = mSampler.getNumVoices() - 1;

    while (!voiceFound && (voiceIndex > -1))
    {
        if (auto v = static_cast<SampleVoice*>(mSampler.getVoice(voiceIndex)))
        {
            if (v->IsPlaying())
            {
                position = v->GetVoicePosition();
                voiceFound = true;
            }
        }
        voiceIndex--;
    }

    return position;
}

void MainProcesser::renderNextBlock(AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    int numSamples = buffer.getNumSamples();
    transportSource.getNextAudioBlock(AudioSourceChannelInfo(&buffer, 0, numSamples));
    mSampler.renderNextBlock(buffer, midiMessages, 0, numSamples);
    FilterAudio(buffer);
    Update(midiMessages,numSamples);
}

void MainProcesser::Update(MidiBuffer& midiMessages, int numSamples)
{
    UpdateTransportSource(midiMessages, numSamples);

    if (mParamChanged)
    {
        if (auto s = static_cast<SampleSound*> (mSampler.getSound(0).get()))
            s->SetSampleParams(mLoadedSample);

        mParamChanged = false;
    }
}

void MainProcesser::UpdateTransportSource(MidiBuffer& midiMessages, int numSamples)
{
    MidiMessage m;
    MidiBuffer::Iterator it{ midiMessages };
    int samplePosition;

    while (it.getNextEvent(m, samplePosition))
    {
        if (m.isNoteOn())
        {
            mIsNotePlayed = true;
            transportSource.setNextReadPosition(0);
        }
        if (m.isNoteOff())
        {
            mIsNotePlayed = false;
            transportSource.setNextReadPosition(0);
        }
    }

    if (mIsNotePlayed)
    {
        transportSource.setNextReadPosition((transportSource.getCurrentPosition() * processer.getSampleRate()) + numSamples);
    }
}

void MainProcesser::FilterAudio(AudioBuffer<float>& buffer)
{
    dsp::AudioBlock<float> block(buffer);
    mLowPassFilter.process(dsp::ProcessContextReplacing<float>(block));
    mHighPassFilter.process(dsp::ProcessContextReplacing<float>(block));
}

#pragma endregion

#pragma region Events


void MainProcesser::LoadFile(const String& path)
{
    File file = File(path);

    if (auto* reader = formatManager.createReaderFor(file))
    {
        std::unique_ptr<AudioFormatReaderSource> newSource(new AudioFormatReaderSource(reader, true));

        if ((reader->lengthInSamples / reader->sampleRate) > MAX_SAMPLE_LENGTH_SECONDS)
        {
            AlertWindow alert("Sorry","Sample Must Be Less Than 15 Seconds", AlertWindow::AlertIconType::WarningIcon, processer.getActiveEditor());
            alert.addButton("Cool", 0);
            alert.runModalLoop();
            return;
        }

        transportSource.setSource(newSource.get(), 0, nullptr, reader->sampleRate);
        thumbnail.setSource(new FileInputSource(file));

        readerSource.reset(newSource.release());

        SampleParams sampleParams(file.getFileName(), reader->sampleRate, reader->lengthInSamples);
        mLoadedSample = sampleParams;

        BigInteger midiRange;
        midiRange.setRange(0, 128, true);

        readerSource->setNextReadPosition(mLoadedSample.startSample);
        transportSource.setNextReadPosition(mLoadedSample.startSample);

        //We Only Ever Want One Sound
        mSampler.clearSounds();
        mSampler.addSound(new SampleSound("Sample", *reader, midiRange, MIDDLE_C, 0.1, 0.1, MAX_SAMPLE_LENGTH_SECONDS, mLoadedSample));
        
    }
}

void MainProcesser::parameterChanged(const String& parameterID, float newValue)
{
    mParamChanged = true;
    //DBG("Parameter Changed" << parameterID);
    std::string s = parameterID.toStdString();

    int pos = s.find("_");

    std::string control = s.substr(0, pos);
    std::string param = s.substr(pos+1);

    if (control == "ENVELOPE")
    {
        HandleEnvelopeChanged(param);
    }
    else if (control == "LOWPASS")
    {
        HandleLowPassChanged(param);
    }
    else if (control == "HIGHPASS")
    {
        HandleHighPassChanged(param);
    }
    else if (control == "TRANSPORT")
    {
        HandleTransportChanged(param);
    }
    else if (control == "BUTTONS")
    {
        HandleButtonPressed(param);
    }
    else if (control == "GAIN")
    {
        mLoadedSample.gain = (float)mAPVTS.getRawParameterValue("GAIN")->load() / 100.f;
    }
}

void MainProcesser::HandleEnvelopeChanged(String param)
{
    if (auto sound = dynamic_cast<SampleSound*>(mSampler.getSound(0).get()))
    {
        if (param == "ATTACK")
        {
            mADSRparams.attack = mAPVTS.getRawParameterValue("ENVELOPE_ATTACK")->load();
        }
        else if (param == "DECAY")
        {
            mADSRparams.decay = mAPVTS.getRawParameterValue("ENVELOPE_DECAY")->load();
        }
        else if (param == "SUSTAIN")
        {
            mADSRparams.sustain = mAPVTS.getRawParameterValue("ENVELOPE_SUSTAIN")->load();
        }
        else if (param == "RELEASE")
        {
            mADSRparams.release = mAPVTS.getRawParameterValue("ENVELOPE_RELEASE")->load();
        }

        sound->setEnvelopeParameters(mADSRparams);
    }
}

void MainProcesser::HandleHighPassChanged(String param)
{
    if (param == "FREQ")
    {
        mHighPassFilter.setCutoffFrequencyHz(mAPVTS.getRawParameterValue("HIGHPASS_FREQ")->load());
    }
    else if (param == "RES")
    {
        mHighPassFilter.setResonance(mAPVTS.getRawParameterValue("HIGHPASS_RES")->load());
    }
    else if (param == "DRIVE")
    {
        mHighPassFilter.setDrive(mAPVTS.getRawParameterValue("HIGHPASS_DRIVE")->load());
    }
}

void MainProcesser::HandleLowPassChanged(String param)
{
    if (param == "FREQ")
    {
        mLowPassFilter.setCutoffFrequencyHz(mAPVTS.getRawParameterValue("LOWPASS_FREQ")->load());
    }
    else if (param == "RES")
    {
        mLowPassFilter.setResonance(mAPVTS.getRawParameterValue("LOWPASS_RES")->load());
    }
    else if (param == "DRIVE")
    {
        mLowPassFilter.setDrive(mAPVTS.getRawParameterValue("LOWPASS_DRIVE")->load());
    }
}

void MainProcesser::HandleTransportChanged(String param)
{
    if (param == "START")
    {
        float percentage = mAPVTS.getRawParameterValue("TRANSPORT_START")->load() / 100.f;
        mLoadedSample.startSample = jmap(percentage, 0.f, (float)mLoadedSample.endSample);
    }
    else if (param == "END")
    {
        float percentage = mAPVTS.getRawParameterValue("TRANSPORT_END")->load() / 100.f;
        mLoadedSample.endSample = jmap(percentage, (float)mLoadedSample.startSample, (float)mLoadedSample.totalSamples);
    }
    else if (param == "SPEED")
    {
        mLoadedSample.playbackSpeed = ((mAPVTS.getRawParameterValue("TRANSPORT_SPEED")->load()) / 100.f) -1;
    }
}

void MainProcesser::HandleButtonPressed(String param)
{
    //DBG("BUTTON PRESSED" << param);
    if (param == "DIRECTION")
    {
        mLoadedSample.isReversed ^= 1;
        if (auto s = static_cast<SampleSound*> (mSampler.getSound(0).get()))
        {
            s->ReverseSound();
            int channels = thumbnail.getNumChannels();;
            thumbnail.reset(channels, mLoadedSample.samplerate, mLoadedSample.totalSamples);
            thumbnail.addBlock(0, *s->getAudioData(), 0, mLoadedSample.totalSamples);
        }
    }
    else if (param == "MODE")
    {
        mLoadedSample.isLooped ^= 1;
    }
    else if (param == "ROLLOFF")
    {
        UpdateFilter();
    }
}

void MainProcesser::changeListenerCallback(ChangeBroadcaster* source)
{
    if (source == &transportSource)
    {
        if (transportSource.isPlaying())
        {
            changeState(Playing);
        }
        else {
            changeState(Stopped);
        }
    }
}

void MainProcesser::changeState(TransportState newState)
{
    if (state != newState)
    {
        state = newState;

        switch (state)
        {

        case Starting:
            mSampler.getVoice(0)->stopNote(0, false);
            transportSource.start();
           // DBG("Starting in change state : " << transportSource.getCurrentPosition());
            break;

        case Playing:
            break;

        case Stopped:
        case Stopping:
            transportSource.stop();
            transportSource.setNextReadPosition(mLoadedSample.startSample);
            //DBG("Stopping in change state : " << transportSource.getCurrentPosition());
            break;

        default:
            jassertfalse;
            break;
        }
    }
}

void MainProcesser::UpdateADSR()
{
    mADSRparams.attack = mAPVTS.getRawParameterValue("ENVELOPE_ATTACK")->load();
    mADSRparams.decay = mAPVTS.getRawParameterValue("ENVELOPE_DECAY")->load();
    mADSRparams.sustain = mAPVTS.getRawParameterValue("ENVELOPE_SUSTAIN")->load();
    mADSRparams.release = mAPVTS.getRawParameterValue("ENVELOPE_RELEASE")->load();

    //mSampler is of type Synthesiser
    //Sound can be of type SamplerSound or SynthesiserSound
    //We need to make sure it is of type sampler, so we check if synamic cast is succsessful
    //if its of type SamplerSound, we can then accsess the correct methods of SamplerSound class
    for (size_t i = 0; i < mSampler.getNumSounds(); ++i)
    {
        if (auto sound = dynamic_cast<SampleSound*>(mSampler.getSound(i).get()))
        {
            sound->setEnvelopeParameters(mADSRparams);
        }
    }
}

void MainProcesser::UpdateFilter()
{
    mLowPassFilter.setMode(mAPVTS.getRawParameterValue("BUTTONS_ROLLOFF")->load() ? dsp::LadderFilter<float>::Mode::LPF12 : dsp::LadderFilter<float>::Mode::LPF24);
    mLowPassFilter.setCutoffFrequencyHz(mAPVTS.getRawParameterValue("LOWPASS_FREQ")->load());
    mLowPassFilter.setResonance(mAPVTS.getRawParameterValue("LOWPASS_RES")->load());
    mLowPassFilter.setDrive(mAPVTS.getRawParameterValue("LOWPASS_DRIVE")->load());

    mHighPassFilter.setMode(mAPVTS.getRawParameterValue("BUTTONS_ROLLOFF")->load() ? dsp::LadderFilter<float>::Mode::HPF12 : dsp::LadderFilter<float>::Mode::HPF24);
    mHighPassFilter.setCutoffFrequencyHz(mAPVTS.getRawParameterValue("HIGHPASS_FREQ")->load());
    mHighPassFilter.setResonance(mAPVTS.getRawParameterValue("HIGHPASS_RES")->load());
    mHighPassFilter.setDrive(mAPVTS.getRawParameterValue("HIGHPASS_DRIVE")->load());
}

#pragma endregion







