#pragma once

#include <JuceHeader.h>
#include "Constants.hpp"
#include "SampleVoice.h"
#include "SampleParams.h"

class SamplerAudioProcessor;

//This Class was Created to Seperate the main processing functinality from the SamplerAudioProcessor class
//The Idea is that the SamplerAudioProcessor class can be responsible for communicating with the DAW and this can be responsible for the main audio processing
//This should improve readbility as the SamplerAudioProcessor class was getting quite large

class MainProcesser : 
	public AudioProcessorValueTreeState::Listener,
	public ChangeListener
{
public:

#pragma region Initilization

	MainProcesser(SamplerAudioProcessor& p);
	~MainProcesser();

	void Initialize();
	void prepareToPlay(double sampleRate, int samplesPerBlock);
	AudioProcessorValueTreeState::ParameterLayout CreateParameters();

	void RegisterListeners();

#pragma endregion

#pragma region Getters
	int GetNumSamplerSounds() { return mSampler.getNumSounds(); };

	SampleParams& GetLoadedSample() { return mLoadedSample; };
	AudioThumbnail& GetAudioThumbnail() { return thumbnail; }

	std::atomic_bool& IsNotePlayed() { return mIsNotePlayed; };
	ADSR::Parameters& GetADSR() { return mADSRparams; };
	AudioProcessorValueTreeState& GetAPVTS() { return mAPVTS; };

	double GetTransportPosition();
#pragma endregion

#pragma region Setters

	void SetLoadedSample(SampleParams params) { mLoadedSample = params; }

#pragma endregion

#pragma region Process

	void renderNextBlock(AudioBuffer<float>& buffer, MidiBuffer& midiMessages);

	void Update(MidiBuffer& midiMessages, int numSamples);

	void UpdateTransportSource(MidiBuffer& midiMessages, int numSamples);

	void FilterAudio(AudioBuffer<float>& buffer);

#pragma endregion

#pragma region Events

	void LoadFile(const String& path);

	void changeListenerCallback(ChangeBroadcaster* source) override;
	
	void parameterChanged(const String& parameterID, float newValue) override;

	void HandleEnvelopeChanged(String param);

	void HandleLowPassChanged(String param);
	void HandleHighPassChanged(String param);

	void HandleTransportChanged(String param);

	void HandleButtonPressed(String param);

#pragma endregion

private:
	enum TransportState { Stopped, Starting, Playing, Stopping };
	void changeState(TransportState newState);

	void UpdateADSR();

	void UpdateFilter();

private:
	Synthesiser mSampler;
	SampleParams mLoadedSample;

	AudioFormatManager formatManager;
	std::unique_ptr<AudioFormatReaderSource> readerSource;
	AudioTransportSource transportSource;
	TransportState state;

	AudioThumbnailCache thumbnailCache;
	AudioThumbnail thumbnail;

	ADSR::Parameters mADSRparams;
	AudioProcessorValueTreeState mAPVTS;

	std::atomic_bool mIsNotePlayed{ false };

	dsp::LadderFilter<float> mLowPassFilter;
	dsp::LadderFilter<float> mHighPassFilter;

	SamplerAudioProcessor& processer;

	bool mParamChanged {true};
};