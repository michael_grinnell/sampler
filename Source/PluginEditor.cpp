#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "Constants.hpp"

SamplerAudioProcessorEditor::SamplerAudioProcessorEditor (SamplerAudioProcessor& p)
    : AudioProcessorEditor (&p),
    mainComponent(p.GetMainProcesser())
{
    setSize(PLUGIN_WIDTH, PLUGIN_HEIGHT);
    addAndMakeVisible(mainComponent);
}

SamplerAudioProcessorEditor::~SamplerAudioProcessorEditor(){}

void SamplerAudioProcessorEditor::paint(Graphics& g)
{
    g.setColour(Colour(BORDER_COLOUR));
    Rectangle<int> componentBorder(0, 0, getWidth(), getHeight());
    g.drawRect(componentBorder, BORDER_LARGE);
}

void SamplerAudioProcessorEditor::resized()
{
    mainComponent.setBounds(BORDER_MED, BORDER_MED, PLUGIN_WIDTH - (BORDER_MED *2), PLUGIN_HEIGHT - (BORDER_MED * 2));
}


