#pragma once

#include <JuceHeader.h>
#include "MainComponent.h"

class SamplerAudioProcessorEditor  : public AudioProcessorEditor
{
public:
    SamplerAudioProcessorEditor (SamplerAudioProcessor& p);
    ~SamplerAudioProcessorEditor();

    void paint (Graphics&) override;
    void resized() override;
   
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SamplerAudioProcessorEditor)

private:
    MainComponent mainComponent;
};
