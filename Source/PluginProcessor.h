/*
  ==============================================================================
  ______________________Resources Used for this Project________________________ 

    This Project was created with the help of The Audio Programmer's YouTube Channel
    and his playlist on creating a VST Sampler
    https://www.youtube.com/playlist?list=PLLgJJsrdwhPyTmhbSVVa5tPmJXJE-OMYm

    This Project was created with the help of The Juce Tutorials
    https://juce.com/learn/tutorials

    Tutorial: Draw audio waveforms
    https://docs.juce.com/master/tutorial_audio_thumbnail.html

    Tutorial: Build an audio player
    https://docs.juce.com/master/tutorial_playing_sound_files.html

    Thank you to Rory Walsh and Dundalk Institute of Technology for teaching the Core Framework,
    and  providing insight and resources for this project.
  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "MainProcesser.h"


class SamplerAudioProcessor : public AudioProcessor
{
public:
#pragma region Constructor / Destructor
    SamplerAudioProcessor();
    ~SamplerAudioProcessor();
#pragma endregion

#pragma region Juce Auto - Generated Code

    void releaseResources() override;

#ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported(const BusesLayout& layouts) const override;
#endif

    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram(int index) override;
    const String getProgramName(int index) override;
    void changeProgramName(int index, const String& newName) override;

    void getStateInformation(MemoryBlock& destData) override;
    void setStateInformation(const void* data, int sizeInBytes) override;

#pragma endregion

    void prepareToPlay(double sampleRate, int samplesPerBlock) override;
    void processBlock(AudioBuffer<float>&, MidiBuffer&) override;

public:
    MainProcesser& GetMainProcesser() { return mProcesser; }

private:
    MainProcesser mProcesser;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SamplerAudioProcessor)
};