#pragma once
#include <JuceHeader.h>

//A Stucture that stores Usfull infomation about the loaded sound
struct SampleParams
{
    String name;
    int samplerate;
    int startSample;
    int endSample;
    int totalSamples;

    float playbackSpeed;
    float currentPos;
    float gain;

    bool isReversed;
    bool isLooped;

    SampleParams(String name, int samplerate, int totalSamples) :
        name(name),
        samplerate(samplerate),
        totalSamples(totalSamples),
        startSample(0),
        endSample(totalSamples),
        isReversed(false),
        isLooped(false),
        playbackSpeed(0),
        currentPos(0),
        gain(0.5f)
    {}
};
