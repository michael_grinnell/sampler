#include "SampleSound.h"

SampleSound::~SampleSound(){}

SampleSound::SampleSound(const String& soundName,
    AudioFormatReader& source,
    const BigInteger& notes,
    int midiNoteForNormalPitch,
    double attackTimeSecs,
    double releaseTimeSecs,
    double maxSampleLengthSeconds,
    SampleParams& s)
    : name(soundName),
    sourceSampleRate(source.sampleRate),
    midiNotes(notes),
    midiRootNote(midiNoteForNormalPitch),
    mSampleParams(s)
{
    if (sourceSampleRate > 0 && source.lengthInSamples > 0)
    {
        length = jmin((int)source.lengthInSamples,
            (int)(maxSampleLengthSeconds * sourceSampleRate));

        data.reset(new AudioBuffer<float>(jmin(2, (int)source.numChannels), length + 4));

        source.read(data.get(), 0, length + 4, 0, true, true);

        params.attack = static_cast<float> (attackTimeSecs);
        params.release = static_cast<float> (releaseTimeSecs);
    }
}

bool SampleSound::appliesToNote(int midiNoteNumber)
{
    return midiNotes[midiNoteNumber];
}

bool SampleSound::appliesToChannel(int /*midiChannel*/)
{
    return true;
}

void SampleSound::ReverseSound()
{
    mSampleParams.isReversed ^= 1;
    data->reverse(0, data->getNumSamples());
}