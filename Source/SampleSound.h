#pragma once

#include <JuceHeader.h>
#include "SampleParams.h"

//This class was Built from examining the Juce SamplerSound Class
//I needed to extend functinality to allow manipulation of the transport controls
//Such as the Start/End position and the playback speed
    class SampleSound : public SynthesiserSound
    {
    public:
        SampleSound(const String& name,
            AudioFormatReader& source,
            const BigInteger& midiNotes,
            int midiNoteForNormalPitch,
            double attackTimeSecs,
            double releaseTimeSecs,
            double maxSampleLengthSeconds,
            SampleParams& s);

        ~SampleSound() override;

        const String& getName() const noexcept { return name; }
        const std::shared_ptr<AudioBuffer<float>> getAudioData() const noexcept { return data; }
        void setEnvelopeParameters(ADSR::Parameters parametersToUse) { params = parametersToUse; }

        bool appliesToNote(int midiNoteNumber) override;
        bool appliesToChannel(int midiChannel) override;

        void ReverseSound();

        void SetSampleParams(SampleParams& params) { mSampleParams = params; };

    private:
        friend class SampleVoice;

        String name;
        int length{ 0 };
        int midiRootNote{ 0 };
        double sourceSampleRate{ 0 };

        BigInteger midiNotes;
        ADSR::Parameters params;
        std::shared_ptr<AudioBuffer<float>> data;

        SampleParams& mSampleParams;

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SampleSound)
    };
