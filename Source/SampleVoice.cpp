#include "SampleVoice.h"

SampleVoice::SampleVoice(){}

SampleVoice::~SampleVoice(){}

bool SampleVoice::canPlaySound(SynthesiserSound* sound)
{
    return dynamic_cast<const SampleSound*> (sound) != nullptr;
}

void SampleVoice::startNote(int midiNoteNumber, float velocity, SynthesiserSound* s, int /*currentPitchWheelPosition*/)
{
    isPlaying = true;
    if (auto* sound = dynamic_cast<const SampleSound*> (s))
    {
        pitchRatio = std::pow(2.0, (midiNoteNumber - sound->midiRootNote) / 12.0)
            * sound->sourceSampleRate / getSampleRate();

        sourceSamplePosition = 0.0;
        lgain = velocity;
        rgain = velocity;

        adsr.setSampleRate(sound->sourceSampleRate);
        adsr.setParameters(sound->params);

        adsr.noteOn();
    }
    else
    {
        jassertfalse; // this object can only play SamplerSounds!
    }
}

void SampleVoice::stopNote(float /*velocity*/, bool allowTailOff)
{
    isPlaying = false;
    if (allowTailOff)
    {
        adsr.noteOff();
    }
    else
    {
        clearCurrentNote();
        adsr.reset();
    }
   
}

void SampleVoice::renderNextBlock(AudioBuffer<float>& outputBuffer, int startSample, int numSamples)
{
    if (auto* playingSound = static_cast<SampleSound*> (getCurrentlyPlayingSound().get()))
    {
        //Set parameters to loacal before Update, to prevent them from being changed during and Update
        int startSampleOffSet = playingSound->mSampleParams.startSample;
        int endSample = playingSound->mSampleParams.endSample;
        bool loop = playingSound->mSampleParams.isLooped;
        float vol = playingSound->mSampleParams.gain;
        float speed = playingSound->mSampleParams.playbackSpeed;



        //Get Read Pointers to Both channels
        auto& data = *playingSound->data;
        const float* const inL = data.getReadPointer(0);
        const float* const inR = data.getNumChannels() > 1 ? data.getReadPointer(1) : nullptr;

        //Get Write Pointers to Both Output Channels
        float* outL = outputBuffer.getWritePointer(0, startSample);
        float* outR = outputBuffer.getNumChannels() > 1 ? outputBuffer.getWritePointer(1, startSample) : nullptr;

        //Loop Through the current Block
        while (--numSamples >= 0)
        {
            //Check If We are at the end of the buffer and handle approprietly
            //Check this First as We allow changing of start and end time
            //this will stop the sound if the position goes outside the bounds
            if (sourceSamplePosition + startSampleOffSet >= endSample)
            {
                if (loop)
                {
                    sourceSamplePosition = 0;
                }
                else
                {
                    stopNote(0.0f, false);
                    break;
                }
            }

            auto pos = (int)sourceSamplePosition + startSampleOffSet;
            auto alpha = (float)(sourceSamplePosition - pos + startSampleOffSet);
            auto invAlpha = 1.0f - alpha;

            float l = (inL[pos] * invAlpha + inL[pos + 1] * alpha);
            float r =  (inR != nullptr) ? (inR[pos] * invAlpha + inR[pos + 1] * alpha) : l;

            //Set Gain For Booth Chanels
            auto envelopeValue = adsr.getNextSample();
            l *= lgain * envelopeValue * vol;
            r *= rgain * envelopeValue * vol;

            if (outR != nullptr)
            {
                *outL++ += l;
                *outR++ += r;
            }
            else
            {
                *outL++ += (l + r) * 0.5f;
            }

            sourceSamplePosition += pitchRatio;
            sourceSamplePosition += speed;
        }
    }
}
bool SampleVoice::IsPlaying()
{
    return isPlaying;
}
double SampleVoice::GetVoicePosition()
{
    return double(sourceSamplePosition + startSampleOffSet) / getSampleRate();
}

