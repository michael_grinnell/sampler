#pragma once

#include <JuceHeader.h>
#include "SampleSound.h"

//This class was Built from examining the Juce SamplerVoice Class
//I needed to extend functinality to allow manipulation of the transport controls
//Such as the Start/End position and the playback speed

class SampleVoice : public SynthesiserVoice
{
public:
	SampleVoice();
	~SampleVoice() override;

    bool canPlaySound(SynthesiserSound*) override;

    void startNote(int midiNoteNumber, float velocity, SynthesiserSound*, int pitchWheel) override;
    void stopNote(float velocity, bool allowTailOff) override;

    void pitchWheelMoved(int newValue) override {};
    void controllerMoved(int controllerNumber, int newValue) override {};

    void renderNextBlock(AudioBuffer<float>&, int startSample, int numSamples) override;

	bool IsPlaying();

    void SetADSR(ADSR env) { adsr = env; }; 

    double GetVoicePosition();

private:
    double pitchRatio{ 0 };
    double sourceSamplePosition{ 0 };
    int startSampleOffSet{ 0 };
    float lgain{ 0 }; 
    float rgain{ 0 };

    ADSR adsr;

    bool isPlaying{ false };
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SampleVoice)
};