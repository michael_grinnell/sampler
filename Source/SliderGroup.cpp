#include <JuceHeader.h>
#include "SliderGroup.h"
#include "Constants.hpp"

SliderGroup::SliderGroup(MainProcesser& p, String name) : processor(p), mName(name){}

SliderGroup::~SliderGroup(){}

#pragma region Prepare

void SliderGroup::SetSliders(std::vector<String> names)
{
    for (size_t i = 0; i < names.size(); ++i)
    {
        SetSlider(i);
        SetLabel(names[i], i);
        mSliderAttachments.add(new AudioProcessorValueTreeState::SliderAttachment(processor.GetAPVTS(), (mName + "_" + mLabels[i]->GetText()).toUpperCase(), *mSliders[i]));
    }
}

void SliderGroup::SetSlider(int i)
{
    mSliders.add(std::unique_ptr<Slider>(new Slider()));
    mSliders[i]->setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
    mSliders[i]->setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
    mSliders[i]->setPopupDisplayEnabled(true, false, getParentComponent());

    addAndMakeVisible(mSliders[i]);
}

void SliderGroup::SetLabel(String name, int i)
{
    std::string s = name.toStdString();
    int pos = s.find(" ");

    mLabels.add(std::unique_ptr<TextLabel>(new TextLabel()));
    mLabels[i]->SetText(s.substr(0, pos));
    mSliders[i]->setTextValueSuffix(s.substr(pos + 1));
    addAndMakeVisible(mLabels[i]);
}

#pragma endregion

#pragma region Process

void SliderGroup::paint(Graphics& g)
{
    g.fillAll(Colour(COMPONENT_BG_COLOUR));

    g.setColour(Colour(BG_COLOUR));
    g.fillRect(0.f, 0.f, (float)getWidth(), getHeight() * 0.15f);

    g.setColour(Colour(BORDER_COLOUR));
    g.drawRect(0.f, 0.f, (float)getWidth(), getHeight() * 0.15f);

    g.setColour(Colour(TEXT_COLOUR));
    g.setFont(FONT_MED);

    g.drawText(mName, 0, 0, getWidth(), getHeight() * 0.15f, Justification::centred);

    g.setColour(Colour(BORDER_COLOUR));
    g.drawRect(0, 0, getWidth(), getHeight());
}

void SliderGroup::resized()
{
    //this is where you could account for multiple rows of sliders
    // for the purpose of this there is no need to impliment
    float sliderWidth = (float)getWidth() / (float)mSliders.size();
    float sliderHeight = (float)getHeight() * 0.50f;

    for (size_t i = 0; i < mSliders.size(); ++i)
    {
        mSliders[i]->setBounds(sliderWidth * i, getHeight() * 0.25f, sliderWidth, sliderHeight);
        mLabels[i]->setBounds((sliderWidth * i) + 10, (sliderHeight)+(getHeight() * 0.25f), (sliderWidth - 20), 15);
    }
}

#pragma endregion

