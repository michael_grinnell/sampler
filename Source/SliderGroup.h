#pragma once

#include <JuceHeader.h>
#include "MainProcesser.h"
#include "TextLabel.h"

//This Class Stores a group of Sliders and associated labels
//It handles positioning of slider components equal to each other
//It also makes adding new groups of sliders Simpler to impliment
// you just pass the slidergroup a name, and a set of names for the each slider
// Then you attach the names to the processer Tree in the plugin Processer

class SliderGroup    : public Component
{
public:
    SliderGroup(MainProcesser& p, String name);
    ~SliderGroup();

    void paint (Graphics&) override;
    void resized() override;

    void SetSliders(std::vector<String> names);
    void SetSlider(int i);
    void SetLabel(String name, int i);

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SliderGroup)

    MainProcesser& processor;
    String mName;

    OwnedArray<Slider> mSliders;
    OwnedArray<TextLabel> mLabels;
    OwnedArray<AudioProcessorValueTreeState::SliderAttachment> mSliderAttachments;
};
