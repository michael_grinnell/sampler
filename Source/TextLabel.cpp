#include <JuceHeader.h>
#include "TextLabel.h"
#include "Constants.hpp"

TextLabel::TextLabel(){}

TextLabel::~TextLabel(){}

void TextLabel::paint (Graphics& g)
{
    g.fillAll(Colour(BG_COLOUR));
    g.setFont(FONT_SMALL);

    g.setColour(Colour(COMPONENT_BG_COLOUR_LIGHT));
    g.fillRect(0,0, getWidth(), getHeight());

    g.setColour(Colour(BORDER_COLOUR));
    g.drawRect(0, 0, getWidth(), getHeight());

    g.setColour(Colour(TEXT_COLOUR));
    g.drawText(mText, 0, 0, getWidth(), getHeight(), Justification::centred);
}

void TextLabel::SetText(String text)
{
    mText = text;
}

void TextLabel::resized(){}
