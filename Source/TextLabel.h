#pragma once
#include <JuceHeader.h>

// A simple class that was created to give a text Label to a component
// The built in class has limited positioning options

class TextLabel    : public Component
{
public:
    TextLabel();
    ~TextLabel();

    void paint(Graphics&) override;
    void resized() override;

    void SetText(String text);
    String GetText() { return mText; };
   
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TextLabel)
    String mText;
};
